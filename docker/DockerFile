FROM nvidia/cuda:12.3.2-cudnn9-devel-ubuntu22.04
# Сурсы https://gitlab.com/nvidia/container-images/cuda/-/tree/master/dist/12.3.2/ubuntu2204/devel?ref_type=heads
# Важно использовать именно cudnnX-devel образы. Только с ними работает Tensorflow.

USER root

############################################## GLOBAL SETTINGS #########################################################
ENV DEBIAN_FRONTEND=noninteractive
ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV TZ=Europe/Moscow
########################################################################################################################


########################################### SYSTEM UPDATE ##############################################################
RUN apt update && apt upgrade -y
########################################################################################################################


############################################### LOCALES ################################################################
ENV SYSTEM_LANG=ru_RU.UTF-8
RUN apt install -y language-pack-ru tzdata && update-locale LANG=$SYSTEM_LANG
########################################################################################################################


############################################### BASE DEPENDENCIES ######################################################
RUN apt install -y sudo bash curl git software-properties-common
########################################################################################################################


################################################### VSCODE #############################################################
# https://github.com/coder/code-server
# https://coder.com/docs/code-server/latest/install#debian-ubuntu
RUN curl -fsSL https://code-server.dev/install.sh | sh
########################################################################################################################


################################################### PYTHON #############################################################
# Добавляем репозиторий с питоном
RUN add-apt-repository ppa:deadsnakes/ppa
# Установка с подзависимсотями
RUN apt install -y python3.12 python3.12-venv python3.12-dev python3.12-gdbm
# Алиас для удобства
RUN ln -s /usr/bin/python3.12 /usr/bin/python && alias python='/usr/bin/python3.12' && alias python3='/usr/bin/python3.12'
# Устанавливаем pip
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3.12
# Обновляемся
RUN python3.12 -m pip install --upgrade pip setuptools wheel
########################################################################################################################


################################################## CLEAN UP ############################################################
RUN apt -y clean && apt -y autoremove && apt -y autoclean && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*
########################################################################################################################

# Bootstrap
COPY ./docker/bootstrap.sh /opt/bootstrap.sh
RUN chmod +x /opt/bootstrap.sh

CMD /opt/bootstrap.sh