
# DSGW - Data Science GPU Workplace

![workplace.png](docs/workplace.png)

Проект призван упростить разворот полноценного рабочего места на сервере с GPU для Data Science разработки

- Проект базируется на:
    - Ubuntu 22.04
    - Nvidia CUDA 12.3.2
    - Nvidia cuDNN 9
    - Python 3.12
    - VsCode Server

## Разворот на сервере

#### Для поддержки gpu из контейнер на стороне хостовой ОС выполнить:

```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-docker-keyring.gpg

curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-docker-keyring.gpg] https://#g' | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt update && sudo apt install -y nvidia-docker2
```
#### Инструкция по сборке контейнера

- В хостовой системе должен быть установлен Nvidia драйвер.

- Для каждого пользователя (если их несколько в одном кластере) необходимо собрать свой экземпляр рабочего места.

- В секции ports в [docker-compose.yml](./docker-compose.yml) порт 8009 должен проксироваться на уникальный порт хостовой машины. Каждому экземпляру нужно прописать "свой_уникальный_порт:8009".

- Собраться и запуститься
    ```
    docker compose up --build
    ```
- Положить
    ```
    docker compose down -v
    ```

## Пользователю развернутого экземпляра

- На сервере предустановлены python, venv, pip
- Предустановлен git и openssh
- Добавлена поддерджка NVIDIA, CUDA, cuDNN
- Установлен VsCode Server
- Пользователю выдаются root привелегии.
- Монтируются в volume настройки vscode и /home/. Сохранность всего что не относится к ним после перезапуска сервера не гарантируется.
- Контейнер не обладает монитором. Tkinter и все что разработано под настольные оболчки может не работать частично или вовсе. Теоретически все что работало с Jupiter - заработает и в VsCode. Рекомендуется установка headless версий пакетов.
- В контейнере пример профиля ([python_dev.code-profile](./home/python_dev.code-profile)) с шорткатами, настройками, плагинами для python раззработки в VsCode если нет времени разбираться самим. Его можно импортировать в ручную из home директории после сборки.
- Пароль меняется либо в репозитории по пути [config.yaml](.config/code-server/config.yaml) либо на сервере [config.yaml](/root/.config/code-server/config.yaml) в секции password. После смены необходим перезапуск VsCode.

#### Работа с окружениями Python
```
# Создать окружение
python -m venv name_of_venv_folder

# Активировать коружение
source name_of_venv_folder/bin/activate

# Выйти из окружения
deactivate
```

## Установка библиотек:

#### - Tensorflow. Отслеживать соместимость с CUDA и cuDNN здесь: 
https://www.tensorflow.org/install/source#gpu
```
pip3 install tensorflow==2.16.1
```

#### - Torch. Команду установки последней совместимой версии можно собрать в форме:
https://pytorch.org/get-started/locally/

```
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu121
```


## License

This project is licensed under the terms of the MIT license.